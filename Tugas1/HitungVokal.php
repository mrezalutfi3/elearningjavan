<!DOCTYPE html>
<html>
    <title>Hitung Vokal</title>
    <body>  
    <form method="post"> 
        <input type="text" name="string" />
        <input type="submit" name="submit" value="Submit">  
    </form>
</body>  
</html> 
<?php 
 
	if(isset($_POST['submit'])) {
		$string = strtolower($_POST['string']);//mengubah string menjadi lowercase
		$num  = preg_match_all('/[aeiou]/i',$string,$matches);//menghitung jumlah huruf vokal pada string
        $vokal = preg_replace("/[bcdfghjklmnpqrstvwxyz0123456789]/", "", $string);//menghapus huruf konsonan dan number pada string
        $vokal = count_chars($vokal, 3);//menghitung huruf vokal yang unique
        $pecahvokal = str_split($vokal);//memecah karakter pada $vokal
        $hasil = "";
        if(strlen($vokal) == 5) { 
            $hasil = $pecahvokal[0] ." dan ". $pecahvokal[1] ." dan ". $pecahvokal[2] ." dan ". $pecahvokal[3] ." dan ". $pecahvokal[4];
        } elseif(strlen($vokal) == 4) {
            $hasil = $pecahvokal[0] ." dan ". $pecahvokal[1] ." dan ". $pecahvokal[2] ." dan ". $pecahvokal[3];
        } elseif(strlen($vokal) == 3) {
            $hasil = $pecahvokal[0] ." dan ". $pecahvokal[1] ." dan ". $pecahvokal[2];
        } elseif(strlen($vokal) == 2) {
            $hasil = $pecahvokal[0] ." dan ". $pecahvokal[1];
        } elseif(strlen($vokal) == 1) {
            $hasil = $pecahvokal[0];
        }//mengecek jumlah huruf pada $pecahvokal untuk menyesuaikan output $hasil

        if ($hasil != NULL) {
            echo "terdapat ". $num ." huruf vokal pada \"". $string ."\" yaitu ". $hasil;
        } else {
            echo "inputan kosong!";
        }
	}
 
?>