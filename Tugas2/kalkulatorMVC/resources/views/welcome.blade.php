<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css">
        <title>Kalkulator MVC</title>
    </head>
    <body>
        <h1>Kalkulator</h1>
        <form>
            <input type="number" name="numA">
            <select name="operator">
                <option selected disabled>Operator</option>
                <option value="+">+</option>
                <option value="-">-</option>
                <option value="*">*</option>
                <option value="/">/</option>
            </select>
            <input type="number" name="numB">
            <div style="margin-top: 1rem">
                <button type="button" onclick="location.href = '?clear'">Hapus</button>
                <button type="submit" name="submit" value="Submit">Hitung</button>
            </div>
        </form>
    </body>
</html>
<?php
    if ($_GET):
        $numA = (double) @$_GET['numA'];
        $numB = (double) @$_GET['numB'];
        $operator = @$_GET['operator'];
        $hasil = 0;

        switch ($operator) {
            case '+':
                $hasil = $numA + $numB;
                break;
            case '-':
                $hasil = $numA - $numB;
                break;
            case '*':
                $hasil = $numA * $numB;
                break;
            case '/':
                $hasil = $numA / $numB;
                break;
        }
    echo "Hasil: ". $hasil;
?>
<?php
endif; 
?>