<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="style.css">

        <title>Hitung Huruf Vokal MVC</title>
    </head>
    <body>  
        <form> 
            <input type="text" name="string" />
            <input type="submit" name="submit" value="Submit">  
        </form>
    </body>  
</html> 
<?php 
     
    if($_GET):
        $string = @$_GET['string'];
        $string = htmlspecialchars($string);//validasi XSS
        $string = strip_tags($string);//validasi XSS
        $string = strtolower($string);//mengubah string menjadi lowercase
        $num  = preg_match_all('/[aeiou]/i',$string,$matches);//menghitung jumlah huruf vokal pada string
        $vokal = preg_replace("/[bcdfghjklmnpqrstvwxyz0123456789]/", "", $string);//menghapus huruf konsonan dan number pada string
        $vokal = count_chars($vokal, 3);//menghitung huruf vokal yang unique
        $pecahvokal = str_split($vokal);//memecah karakter pada $vokal
        $hasil = "";

        if(strlen($vokal) == 5) { 
            $hasil = $pecahvokal[0] ." dan ". $pecahvokal[1] ." dan ". $pecahvokal[2] ." dan ". $pecahvokal[3] ." dan ". $pecahvokal[4];
        } elseif(strlen($vokal) == 4) {
            $hasil = $pecahvokal[0] ." dan ". $pecahvokal[1] ." dan ". $pecahvokal[2] ." dan ". $pecahvokal[3];
        } elseif(strlen($vokal) == 3) {
            $hasil = $pecahvokal[0] ." dan ". $pecahvokal[1] ." dan ". $pecahvokal[2];
        } elseif(strlen($vokal) == 2) {
            $hasil = $pecahvokal[0] ." dan ". $pecahvokal[1];
        } elseif(strlen($vokal) == 1) {
            $hasil = $pecahvokal[0];
        }//mengecek jumlah huruf pada $pecahvokal untuk menyesuaikan output $hasil

        if ($hasil != NULL) {
            echo "terdapat ". $num ." huruf vokal pada \"". $string ."\" yaitu ". $hasil;
        } else {//jika inputan kosong atau inputan tidak mengandung huruf vokal
            echo "tidak ada huruf vokal!";
        }

?>
<?php
endif; 
?>