INSERT INTO karyawan VALUES 
    (1,'Rizki Saputra','L','Menikah',STR_TO_DATE('10/11/1980','%m/%d/%Y'),STR_TO_DATE('1/1/2011','%m/%d/%Y'),1),
    (2,'Farhan Reza','L','Belum',STR_TO_DATE('11/11/1989','%m/%d/%Y'),STR_TO_DATE('1/1/2011','%m/%d/%Y'),1),
    (3,'Riyando Adi','L','Menikah',STR_TO_DATE('1/25/1977','%m/%d/%Y'),STR_TO_DATE('1/1/2011','%m/%d/%Y'),1),
    (4,'Diego Manuel','L','Menikah',STR_TO_DATE('2/22/1983','%m/%d/%Y'),STR_TO_DATE('9/4/2012','%m/%d/%Y'),2),
    (5,'Satya Lalsana','L','Menikah',STR_TO_DATE('1/12/1981','%m/%d/%Y'),STR_TO_DATE('3/19/2011','%m/%d/%Y'),2),
    (6,'Miguel Hernandez','L','Menikah',STR_TO_DATE('10/16/1994','%m/%d/%Y'),STR_TO_DATE('6/15/2014','%m/%d/%Y'),2),
    (7,'Putri Persada','P','Menikah',STR_TO_DATE('1/30/1988','%m/%d/%Y'),STR_TO_DATE('4/14/2013','%m/%d/%Y'),2),
    (8,'Alma Safira','P','Menikah',STR_TO_DATE('5/18/1991','%m/%d/%Y'),STR_TO_DATE('9/18/2013','%m/%d/%Y'),3),
    (9,'Haqi Hafiz','L','Belum',STR_TO_DATE('9/19/1995','%m/%d/%Y'),STR_TO_DATE('3/9/2015','%m/%d/%Y'),3),
    (10,'Abi Isyawara','L','Belum',STR_TO_DATE('6/3/1991','%m/%d/%Y'),STR_TO_DATE('1/22/2012','%m/%d/%Y'),3),
    (11,'Maman Kresna','L','Belum',STR_TO_DATE('8/21/1993','%m/%d/%Y'),STR_TO_DATE('9/15/2012','%m/%d/%Y'),3),
    (12,'Nadia Aulia','P','Belum',STR_TO_DATE('10/7/1989','%m/%d/%Y'),STR_TO_DATE('5/7/2012','%m/%d/%Y'),4),
    (13,'Mutiara Rezki','P','Menikah',STR_TO_DATE('3/23/1988','%m/%d/%Y'),STR_TO_DATE('5/21/2013','%m/%d/%Y'),4),
    (14,'Dani Setiawan','L','Belum',STR_TO_DATE('2/11/1986','%m/%d/%Y'),STR_TO_DATE('11/30/2014','%m/%d/%Y'),4),
    (15,'Budi Putra','L','Belum',STR_TO_DATE('10/23/1995','%m/%d/%Y'),STR_TO_DATE('12/3/2015','%m/%d/%Y'),4)

INSERT INTO departemen VALUES
    (1,'Manajemen')
    (2,'Pengembangan Bisnis')
    (3,'Teknisi')
    (4,'Analis')